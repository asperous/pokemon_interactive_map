from lxml import etree
import json
import sys

filename = sys.argv[1]
out_filename = sys.argv[2]
output = {}

with open(filename, 'r') as infile: 
    tree = etree.parse( infile )

root = tree.getroot()

g = root.find("{http://www.w3.org/2000/svg}g")
pageX, pageY = [float(i) for i in g.attrib['transform'][10:-1].split(',')]

texts = g.findall("{http://www.w3.org/2000/svg}text")

def get_output():
	for text in texts:
		d = text.findall("{http://www.w3.org/2000/svg}tspan")[0]
		x, y, t = d.attrib['x'], d.attrib['y'], d.text
		if t:
			yield float(x)+pageX, float(y)+pageY, t



with open(out_filename, 'w') as outfile:
	json.dump(
		list(get_output()),
		outfile,
		sort_keys=True,
		indent=4,
		separators=(',', ': ')
	)

# Prepend "translation = "
with open(out_filename, 'r') as outfile:
	text_data = outfile.read()

with open(out_filename, 'w') as outfile:
	outfile.write("labels = " + text_data)
