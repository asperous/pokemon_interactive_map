var heros = {};

function makeHero() {
    return $('<div class="ico" style="top: 100px; left: 100px;" />');
}

function updateScoreboard(scoreboard, update) {
    var richest = scoreboard.children("#richest");
    var caught = scoreboard.children("#caught");
    var survival = scoreboard.children("#survival");
    richest.children("div").html('<img src="p.svg"/>' + update.richest[1] + "<h2>" + update.richest[0] +"</h2>");
    caught.children("div").html(update.number_caught[1] + " Pkmn<h2>" + update.number_caught[0] +"</h2>");
    survival.children("div").html(update.least_deaths[1] + " KOs<h2>" + update.least_deaths[0] +"</h2>");
}

$(function () {
    var ws = new WebSocket("ws://pkmn.snc.io:9999/test");
    var $map = $('#map');
    var $scoreBoard = $('#scoreboard').children("ul");
    var scale = 800 / 244;
    $.each(labels, function (i, e) {
        var label = $("<h2 class='label'>" + e[2] + "<h2>");
        label.css("left", (e[0] * scale) + "px");
        label.css("top", (e[1] * scale) - 15 + "px");
        if (label.text().match(/[0-9]/))
            label.addClass("route");
        $map.append(label);
    });
    ws.onmessage = function (evt) {
        var d = $.parseJSON(evt.data);
        if (d.hasOwnProperty("least_deaths")) {
            updateScoreboard($scoreBoard, d);
            return;
        }
        var map_name = d.map_bank + "." + d.map_number;
        var direction = d.direction;
        var hero_name = d.name;
        if (!translation.hasOwnProperty(map_name))
            return;
        if (!heros.hasOwnProperty(hero_name)) {
            heros[hero_name] = makeHero();
            heros[hero_name].current_x = 0;
            $map.append(heros[hero_name]);
        }
        var icon = heros[hero_name];
        var tmap = translation[map_name];
        var tmapX = tmap.x * scale;
        var tmapY = tmap.y * scale;
        var tmapH = tmap.height * scale;
        var tmapW = tmap.width * scale;
        var offsetX = (d.x / 100) * tmapW;
        var offsetY = (d.y / 100) * tmapH;
        var sprite_offsetY = 19;
        icon.css("top", (tmapY + offsetY - sprite_offsetY + "px"));
        icon.css("left", (tmapX + offsetX + "px"));
        icon.current_y = direction * 22 * 2;
        icon.moving = 2;
    };
    ws.onopen = function (evt) {
        $('#conn_status').html('<i class="fa fa-toggle-on" aria-hidden="true"></i><span>Connected</span>');
    };
    ws.onerror = function (evt) {
        $('#conn_status').html('<i class="fa fa-toggle-off" aria-hidden="true"></i><span>Error</span>');
    };
    ws.onclose = function (evt) {
        $('#conn_status').html('<i class="fa fa-toggle-off" aria-hidden="true"></i><span>Closed</span>');
    };
    setInterval(function () {
            for (i in heros) {
                var h = heros[i];
                if (h.moving > 0)
                    h.moving -= 1;
                else if (h.moving == 0) {
                    h.css("background-position", 0 + "px " + h.current_y + "px");
                    continue;
                } else
                    continue;
                if (h.current_x <= -16 * 2 * 3)
                    h.current_x = 0;
                else
                    h.current_x -= 16 * 2;
                h.css("background-position", h.current_x + "px " + h.current_y + "px");
            }
        },
        150);
});
