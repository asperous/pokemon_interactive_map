import json
import struct

import gevent
import zmq.green as zmq
from geventwebsocket.handler import WebSocketHandler
from convert_name import convert_name
from collections import namedtuple

msg_format = "B" * 7 + "I" * 3

MsgClass = namedtuple("MsgClass", "x y direction number_caught map_bank map_number deaths money name")

scoreboard = {
    'number_caught': ("", -1),
    'richest': ("", 0),
    'least_deaths': ("", -1),
    'money': {},
    'deaths': {}
}

last_updates = {}


def unpack(msg):
    msg = struct.unpack(msg_format, msg)
    msg = msg[:-2] + (convert_name(msg[-2], msg[-1]),)
    return MsgClass(*msg)


def handle_scoreboard(msg, sock_outgoing):
    send_update = False
    # Caught
    name, number_caught = scoreboard['number_caught']
    if msg.number_caught >= number_caught:
        if msg.name != name and name:
            scoreboard['number_caught'] = "=Tie=", msg.number_caught
        else:
            scoreboard['number_caught'] = msg.name, msg.number_caught
        send_update = True
    # Money
    if msg.money != scoreboard['money'].get(msg.name, -1):
        scoreboard['money'][msg.name] = msg.money
        most_name, most_money_amount = "", 0
        for name, money in scoreboard['money'].items():
            if money == most_money_amount:
                scoreboard['richest'] = "=Tie=", msg.money
            elif money > most_money_amount:
                most_name, most_money_amount = name, money
                scoreboard['richest'] = name, money
            send_update = True
    # Deaths
    if msg.deaths != scoreboard['deaths'].get(msg.name, -1):
        scoreboard['deaths'][msg.name] = msg.deaths
        least_so_far = -1
        for name, current_deaths in scoreboard['deaths'].items():
            if least_so_far < 0 or current_deaths < least_so_far:
                least_so_far = current_deaths
                scoreboard['least_deaths'] = name, current_deaths
                send_update = True
            elif current_deaths == least_so_far:
                scoreboard['least_deaths'] = "=Tie=", current_deaths
    if send_update:
        sock_outgoing.send_pyobj(scoreboard)


def main():
    context = zmq.Context()

    ws_server = gevent.pywsgi.WSGIServer(
        ('', 9999), WebSocketApp(context),
        handler_class=WebSocketHandler)

    ws_server.start()

    zmq_server(context)


def zmq_server(context):
    sock_outgoing = context.socket(zmq.PUB)
    sock_outgoing.bind('inproc://queue')

    sock_incoming = context.socket(zmq.SUB)
    sock_incoming.setsockopt(zmq.SUBSCRIBE, "")
    sock_incoming.bind("tcp://*:5555")

    while True:
        msg = sock_incoming.recv()
        msg = unpack(msg)
        handle_scoreboard(msg, sock_outgoing)
        last_updates[msg.name] = msg._asdict()
        sock_outgoing.send_pyobj(msg._asdict())


class WebSocketApp(object):
    def __init__(self, context):
        self.context = context

    def __call__(self, environ, start_response):
        ws = environ['wsgi.websocket']
        sock = self.context.socket(zmq.SUB)
        sock.setsockopt(zmq.SUBSCRIBE, "")
        sock.connect('inproc://queue')
        for msg in last_updates.values():
            ws.send(json.dumps(msg))
        ws.send(json.dumps(scoreboard))
        while True:
            msg = sock.recv_pyobj()
            ws.send(json.dumps(msg))


if __name__ == '__main__':
    main()
